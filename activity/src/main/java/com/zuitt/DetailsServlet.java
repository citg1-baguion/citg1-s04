package com.zuitt;

import jakarta.servlet.ServletConfig;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
@WebServlet("/details")



public class DetailsServlet extends HttpServlet {
    /**
	 * 
	 */
	private static final long serialVersionUID = 6210978271665144495L;
	
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
		ServletConfig config = getServletConfig();

        
        String firstName = System.getProperty("firstName");
        String lastName = (String) request.getSession().getAttribute("lastName");
        String email = (String) config.getServletContext().getAttribute("email");
        String contactNumber = request.getParameter("contactNumber");

       
        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        out.println("<html>");
        out.println("<head>");
        out.println("<title>User Details</title>");
        out.println("</head>");
        out.println("<body>");
        out.println("<h1> Welcome to Phonebook</h1>");
        out.println("<p><b>First Name:</b> " + firstName + "</p>");
        out.println("<p><b>Last Name:</b> " + lastName + "</p>");
        out.println("<p><b>Email:</b> " + email + "</p>");
        out.println("<p><b>Contact Number:</b> " + contactNumber + "</p>");
        out.println("</body>");
        out.println("</html>");
    }
}
