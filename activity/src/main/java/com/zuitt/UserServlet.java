package com.zuitt;

import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.io.IOException;
@WebServlet("/user")




public class UserServlet extends HttpServlet {
    /**
	 * 
	 */
	private static final long serialVersionUID = 6095907340439418044L;
	/**
	 * 
	 */

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("index.html");
        
    }
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String contactNumber = request.getParameter("contactNumber");

       
        System.setProperty("firstName", firstName);

        
        HttpSession session = request.getSession();
        session.setAttribute("lastName", lastName);

      
        ServletContext context = getServletContext();
        context.setAttribute("email", email);

        response.sendRedirect("details?contactNumber=" + contactNumber);
    }
}
